using { managed, cuid } from '@sap/cds/common';
using {sap.common.CodeList} from '@sap/cds/common';

type EmployeeType: String (1);
type Description: String(1000);
type Name: String(255);
type CompanyTypeCode: String(5);

//Company Types
entity CompanyTypes : CodeList {
  key code : CompanyTypeCode @(title : 'Company Type');
}

annotate CompanyTypes with {
  code @Common.Text : name;
}

type CompanyType : Association to CompanyTypes;

annotate CompanyType with @(title : 'Company Type');


entity Companies: managed, cuid{
  companyName: String(50);
  companyType: CompanyType;
  employees: Composition of many Employees on employees.company = $self;
}

entity Employees: managed, cuid{
  company: Association to Companies;
  firstName: String(30);
  lastName: String(30);
  employeeType: EmployeeType;
  level: Integer;
}