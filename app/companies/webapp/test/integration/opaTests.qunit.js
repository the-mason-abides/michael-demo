sap.ui.require(
    [
        'sap/fe/test/JourneyRunner',
        'companies/test/integration/FirstJourney',
		'companies/test/integration/pages/CompaniesList',
		'companies/test/integration/pages/CompaniesObjectPage',
		'companies/test/integration/pages/EmployeesObjectPage'
    ],
    function(JourneyRunner, opaJourney, CompaniesList, CompaniesObjectPage, EmployeesObjectPage) {
        'use strict';
        var JourneyRunner = new JourneyRunner({
            // start index.html in web folder
            launchUrl: sap.ui.require.toUrl('companies') + '/index.html'
        });

       
        JourneyRunner.run(
            {
                pages: { 
					onTheCompaniesList: CompaniesList,
					onTheCompaniesObjectPage: CompaniesObjectPage,
					onTheEmployeesObjectPage: EmployeesObjectPage
                }
            },
            opaJourney.run
        );
    }
);