using CompanyService as service from '../../srv/company-service';

annotate service.Companies with @(
    UI.LineItem : [
        {
            $Type : 'UI.DataField',
            Label : 'Company Name',
            Value : companyName,
        },
        {
            $Type : 'UI.DataField',
            Label : 'Company Type',
            Value : companyType,
        },
    ]
);
annotate service.Companies with @(
    UI.FieldGroup #GeneratedGroup1 : {
        $Type : 'UI.FieldGroupType',
        Data : [
            {
                $Type : 'UI.DataField',
                Label : 'Company Name',
                Value : companyName,
            },
            {
                $Type : 'UI.DataField',
                Label : 'Company Type',
                Value : companyType,
            },
        ],
    },
    UI.Facets : [
        {
            $Type : 'UI.ReferenceFacet',
            ID : 'GeneratedFacet1',
            Label : 'Company Data',
            Target : '@UI.FieldGroup#GeneratedGroup1',
        },
        {
            $Type  : 'UI.ReferenceFacet',
            Label  : 'Employees',
            Target : 'employees/@UI.LineItem'
        },
    ]
);

annotate service.Employees with @(UI : {
    HeaderInfo          : {
        TypeName       : 'Employee',
        TypeNamePlural : 'Employees',
        Description    : {
            $Type : 'UI.DataField',
            Value : lastName
        }
    },
    SelectionFields     : [],
    LineItem            : [
        {
            $Type : 'UI.DataField',
            Label : 'First Name',
            Value : firstName,
        },
        {
            $Type : 'UI.DataField',
            Label : 'Last Name',
            Value : lastName,
        }
    ],
    Facets              : [{
        $Type  : 'UI.ReferenceFacet',
        Label  : 'Employee Data',
        Target : '@UI.FieldGroup#General'
    }],
    FieldGroup #General : {Data : [
        {
            $Type : 'UI.DataField',
            Label : 'First Name',
            Value : firstName,
        },
        {
            $Type : 'UI.DataField',
            Label : 'Last Name',
            Value : lastName,
        }
    ]}
}, ) {

};
