## Application Details
|               |
| ------------- |
|**Generation Date and Time**<br>Mon Jul 24 2023 07:19:26 GMT-0600 (Mountain Daylight Time)|
|**App Generator**<br>@sap/generator-fiori-elements|
|**App Generator Version**<br>1.10.2|
|**Generation Platform**<br>Visual Studio Code|
|**Template Used**<br>Worklist Page V4|
|**Service Type**<br>Local Cap|
|**Service URL**<br>http://localhost:4004/company/
|**Module Name**<br>companies|
|**Application Title**<br>Companies|
|**Namespace**<br>|
|**UI5 Theme**<br>sap_horizon|
|**UI5 Version**<br>1.116.0|
|**Enable Code Assist Libraries**<br>False|
|**Enable TypeScript**<br>False|
|**Add Eslint configuration**<br>False|
|**Main Entity**<br>Companies|
|**Navigation Entity**<br>employees|

## companies

Michael&#39;s Demo of Companies

### Starting the generated app

-   This app has been generated using the SAP Fiori tools - App Generator, as part of the SAP Fiori tools suite.  In order to launch the generated app, simply start your CAP project and navigate to the following location in your browser:

http://localhost:4004/companies/webapp/index.html

#### Pre-requisites:

1. Active NodeJS LTS (Long Term Support) version and associated supported NPM version.  (See https://nodejs.org)


