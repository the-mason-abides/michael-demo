// @ts-nocheck

const cds = require("@sap/cds/lib")
const { GET, POST, expect } = cds.test(__dirname + "/..")

describe("Company Service", () => {
  it("should promote employee", async () => {

    const companyResultInitial = await GET(`/odata/v4/company/Companies(20466922-7d57-4e76-b14c-e53fd97dcb20)?$expand=employees`)

    const employee = companyResultInitial.data.employees[0]

    await POST('/odata/v4/company/promoteEmployee', {employeeID:employee.ID})

    const companyResultAfter = await GET(`/odata/v4/company/Companies(20466922-7d57-4e76-b14c-e53fd97dcb20)?$expand=employees`)

    const promotedEmployee = companyResultAfter.data.employees[0] 

    expect(promotedEmployee.level).to.equal(employee.level + 1)

  })

  it("should change employee's type", async () => {

    const companyResultInitial = await GET(`/odata/v4/company/Companies(20466922-7d57-4e76-b14c-e53fd97dcb20)?$expand=employees`)

    const employeeType = "M"
    const employee = companyResultInitial.data.employees[0]

    await POST('/odata/v4/company/changeEmployeeType', {employeeID:employee.ID, employeeType:employeeType})

    const companyResultAfter = await GET(`/odata/v4/company/Companies(20466922-7d57-4e76-b14c-e53fd97dcb20)?$expand=employees`)

    const changedEmployee = companyResultAfter.data.employees[0] 

    expect(changedEmployee.employeeType).to.equal(employeeType)
    expect(changedEmployee.level).to.equal(1)

  })
})
