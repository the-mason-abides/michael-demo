const expect = require("chai").expect

const { canAddAddress, canGetAddress, canValidateAddressType } = require("../srv/address-manager")

const STUB_ADDRESS_TYPES = { ADDRESS_TYPE_1: "address-type-1", ADDRESS_TYPE_2: "address-type-2" }

describe("Address Manager", () => {
  it("should validate address type", () => {

    const { validateAddressType } = canValidateAddressType({}, STUB_ADDRESS_TYPES)

    expect(() => validateAddressType("address-type-3")).to.throw(`Invalid Address Type "address-type-3"`)

    expect(() => validateAddressType("address-type-2")).to.not.throw(`Invalid Address Type "address-type-2"`)
  })

  it("should add and retrieve appropriate address", () => {
    const expectedAddress = { addressType: "address-type-1", street: "Test Street", streetNumber: 123, region: "Test Region", country: "Test Country", addressCode: "1234567" }

    const stubState = {addresses: []}

    const { addAddress } = canAddAddress(stubState, { validateAddressType: function() {}} )
    const { getAddress } = canGetAddress(stubState, { validateAddressType: function() {}} )

    addAddress(STUB_ADDRESS_TYPES.ADDRESS_TYPE_1, "Test Street", 123, "Test Region", "Test Country", "1234567")
    const address = getAddress(STUB_ADDRESS_TYPES.ADDRESS_TYPE_1)

    expect(address).to.deep.equal(expectedAddress)

    expect(() => getAddress(STUB_ADDRESS_TYPES.ADDRESS_TYPE_2)).to.throw(`No address exists for Address Type "${STUB_ADDRESS_TYPES.ADDRESS_TYPE_2}"`)

  })


})
