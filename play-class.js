// function Employee(firstName, lastName) {
//     this.firstName = firstName
//     this.lastName = lastName
//     this.getFullName = function () {
//       return this.firstName + ` ` + this.lastName
  
//   }
// }

// const michael = new Employee("Michael", "Tews")

// console.log(michael.getFullName())

// const steven = new Employee("Steven", "Mason")
// console.log(steven.getFullName())


class Employee{

  constructor(firstName, lastName) {
    this.firstName = firstName
    this.lastName = lastName
  }

  getFullName(){
    return this.firstName + ` ` + this.lastName
  }

}

class Manager extends Employee {

  #department

  constructor(firstName, lastName, department){
    super(firstName, lastName)
    this.#department = department
  }

  getDepartment(){
    return this.#department
  }

  #getLastName(){
    return this.lastName
  }
}

const michael = new Manager("Michael", "Tews", "IT")

console.log(michael.getFullName())
// can't do this because of private variable declared with #
//michael.#department = "HR"
//michael.#getLastName()
console.log(michael.getDepartment())

const steven = new Employee("Steven", "Mason")
console.log(steven.getFullName())