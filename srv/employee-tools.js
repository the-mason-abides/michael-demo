const employeeTypeConfig = [
  {
    employeeType: "I",
    description: "Individual Contributor",
    maxLevels: 6,
  },
  {
    employeeType: "M",
    description: "Manager",
    maxLevels: 4,
  },
]

const canGetEmployeeTypes = (env, state) => {
  return {
    getEmployeeTypes: () => {
      return state.employeeTypeConfig.map((item) => {
        return { employeeType: item.employeeType, description: item.description }
      })
    },
  }
}

/**
 *
 * @param {{cds:object}} env
 * @param {{employeeTypeConfig:object, employeeEntity:object}} state
 * @returns
 */
const canPromote = (env, state) => {
  return {
    promote: async (employeeID) => {
      const employeeRecord = await env.cds.ql.SELECT.one.from(state.employeeEntity).columns("level", "employeeType").where({ ID: employeeID })
      if (!employeeRecord) throw new Error(`Employee ID ${employeeID} is not valid`)

      const employeeType = state.employeeTypeConfig.find((item) => {
        return item.employeeType === employeeRecord.employeeType
      })
      if (employeeRecord.level === employeeType.maxLevels)
        throw new Error(`Employee is at max level of ${employeeType.maxLevels} for employee type ${employeeType.description}`)

      const newLevel = employeeRecord.level + 1
      await env.cds.ql.UPDATE(state.employeeEntity, employeeID).with({ level: newLevel })
      return newLevel
    },
  }
}

/**
 *
 * @param {{cds:object}} env
 * @param {{employeeEntity:object}} state
 * @returns
 */
const canChangeEmployeeType = (env, state) => {
  return {
    changeEmployeeType: async (employeeID, employeeType) => {
      const employeeRecord = await env.cds.ql.SELECT.one.from(state.employeeEntity).columns("level", "employeeType").where({ ID: employeeID })
      if (!employeeRecord) throw new Error(`Employee ID ${employeeID} is not valid`)
      

      await env.cds.ql.UPDATE(state.employeeEntity, employeeID).with({ level: 1, employeeType: employeeType })
    },
  }
}

const composeEmpoyeeTools = (env) => {
  const { Employees } = env.cds.entities

  const state = { employeeTypeConfig, employeeEntity: Employees }

  return {
    ...canGetEmployeeTypes(env, state),
    ...canPromote(env, state),
    ...canChangeEmployeeType(env, state),
  }
}

module.exports = { composeEmpoyeeTools }
