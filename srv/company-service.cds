using {
  Companies as Companiesdb,
               EmployeeType,
               Description,
               Name,
} from '../db/schema';

service CompanyService {

  entity Companies as projection on Companiesdb;
  annotate Companies with @data.draft.enabled;

  entity EmployeeTypes {
    employeeType : EmployeeType;
    description : Description;
  }

  action promoteEmployee(employeeID : UUID) returns Integer;
  action changeEmployeeType(employeeID : UUID, employeeType:EmployeeType);


}
