const canGetFullName = (state) => {
  return {
    getFullName: () => {
      return state.firstName + ` ` + state.lastName
    }
  }
}

const canSetFirstName = (state) => {
  return {
    setFirstName: (newFirstName) => {
      state.firstName = newFirstName
    }
  }
}

const canSetLastName = (state) => {
  return {
    setLastName: (newLastName) => {
      state.lastName = newLastName
    }
  }
}

const canSetAge = (state) => {
  return {
    setAge: (newAge) => {
      state.age = newAge
    }
  }
}

const canGetAge = (state) => {
  return {
    getAge: () => {
      return state.age
    }
  }
}

const canBirthday = (state, { setAge }, { getAge } ) => {
  return {
    birthday: () => {
      setAge(getAge() * 10)
    }
  }
}

/**
 * @param {{employmentLevel:string|null}} state
 */
const canSetEmployementLevel = (state) => {
  return {
    /**
     * 
     * @param {string} newEmploymentLevel 
     */
    setEmployementLevel: (newEmploymentLevel) => {
      state.employmentLevel = newEmploymentLevel
    }
  }
}


const composeEmployee = (firstName,lastName, age) => {
  const state = {
    firstName,
    lastName,
    age,
    employmentLevel: null
  }

  return {
    ...canGetAge(state),
    ...canBirthday(state, canSetAge(state), canGetAge(state)),
    ...canGetFullName(state),
    ...canSetFirstName(state),
    ...canSetLastName(state),
    ...canSetEmployementLevel(state)
  }
}

module.exports = { composeEmployee }