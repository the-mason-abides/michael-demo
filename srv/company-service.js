const cds = require("@sap/cds")
const { composeEnvironment } = require("./env")
const { composeEmpoyeeTools } = require("./employee-tools")

module.exports = cds.service.impl(async (srv) => {
  const { EmployeeTypes, Employees } = srv.entities

  srv.on("READ", EmployeeTypes, (req) => {
    
    try{
      const env = composeEnvironment(cds, srv, req.locale)
      const employeeTools = composeEmpoyeeTools(env)
      const employeeTypes = employeeTools.getEmployeeTypes()
      return employeeTypes
    }catch(error){
      req.reject(error)
    }

  })

  srv.on("promoteEmployee", async (req) => {
    const { employeeID } = req.data

    try{
      const env = composeEnvironment(cds, srv, req.locale)
      const employeeTools = composeEmpoyeeTools(env)
      const employeeLevel = await employeeTools.promote(employeeID)
      return employeeLevel
    }catch(error){
      req.reject(error)
    }


  })

  srv.on("changeEmployeeType", async (req) => {
    const { employeeID, employeeType } = req.data

    try{
      const env = composeEnvironment(cds, srv, req.locale)
      const employeeTools = composeEmpoyeeTools(env)
      await employeeTools.changeEmployeeType(employeeID, employeeType)
    }catch(error){
      req.reject(error)
    }
  })
})
