
const ADDRESS_TYPES = { HOME: "home", MAILING: "mailing", BUSINESS: "business"}


const canValidateAddressType = (state, ADDRESS_TYPES) => {
  return {
    validateAddressType: (addressType) => {
      const addressTypesArray = Object.entries(ADDRESS_TYPES)
      const addressTypes = addressTypesArray.map(item => item[1])
      
      if(!addressTypes.includes(addressType)) throw new Error(`Invalid Address Type "${addressType}"`)
    }
  }
}

/**
 * 
 * @param {{addresses:{addressType: string, street:string, streetNumber:number, region:string, country:string, addressCode: string}[]}} state 
 * @param {{validateAddressType:function}} validateAddressType
 * @returns 
 */
const canAddAddress = (state, { validateAddressType }) => {
  return {
    /**
     * 
     * @param {string} addressType 
     * @param {string} street 
     * @param {number} streetNumber 
     * @param {string} region 
     * @param {string} country 
     * @param {string} addressCode
     */
    addAddress: (addressType, street, streetNumber, region, country, addressCode) => {

      validateAddressType(addressType)

      const usedAddressTypes = state.addresses.map((item) => item.addressType)
      if(usedAddressTypes.includes(addressType)) throw new Error (`Address Type "${addressType}" already used`)

      state.addresses.push({addressType, street, streetNumber, region, country, addressCode})
    }
  }
}

/**
 * 
 * @param {{addresses:{addressType: string, street:string, streetNumber:number, region:string, country:string, addressCode: string}[]}} state 
 * @param {{validateAddressType:function}} validateAddressType
 * @returns 
 */
const canGetAddress = (state, { validateAddressType }) => {
  return {
    getAddress: (addressType) => {

      validateAddressType(addressType)

      const address = state.addresses.find((item) => item.addressType === addressType)
      if(!address) throw new Error(`No address exists for Address Type "${addressType}"`)

      return address
    }
  }
}

const composeAddressManager = () => {

  const state = {
    /**
     * @type {{addressType: string, street:string, streetNumber:number, region:string, country:string, addressCode: string}[]}
     */
    addresses: []
  }

  return {
    ...canAddAddress(state, canValidateAddressType(state, ADDRESS_TYPES)),
    ...canGetAddress(state, canValidateAddressType(state, ADDRESS_TYPES))
  }

}

module.exports = { composeAddressManager, ADDRESS_TYPES, canValidateAddressType, canAddAddress, canGetAddress }
